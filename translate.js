class Translate{
  
   constructor(){  
    this.apikey="trnsl.1.1.20181119T221410Z.28214b58100fdcdf.bc7625f9c3bcdc36be053178100e0ca5849d69a6";  
    }
    translate(text,dil){
        this.text = text;
        this.dil = dil;
        return this.cevirolum();
    }
    cevirolum(){
        let url = `https://translate.yandex.net/api/v1.5/tr.json/translate?key=${this.apikey}&lang=${this.dil}&text=${this.text}`
        return new Promise((resolve,reject)=>{
            fetch(url).then((res)=>res.json()).then(res=>resolve(res))
        })
    }
}