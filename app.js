const text = document.getElementById("text");
const dil = document.getElementById("dil");
const ceviri = document.getElementById("translate");
const buton = document.getElementById("cevir");
const translate = new Translate();
events();
function events(){
    buton.addEventListener("click",translater);
}
function translater(){
    //Cevirecek ve ui'da yazacak
    let metin = text.value;
    let seciliDil = dil.options[dil.selectedIndex].value;
    let cevrilmis = translate.translate(metin,seciliDil);
    cevrilmis.then(res=>ceviri.value = res.text)
}